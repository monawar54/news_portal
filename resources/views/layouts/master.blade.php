
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.univertheme.com/maxnews/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Mar 2018 03:48:13 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{asset('frontend/dist/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/dist/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">

    <!-- Optional theme -->
    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" id="themify-icons-css" href="{{asset('frontend/css/themify-icons.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/fonts.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <link rel="shortcut icon" href="{{asset('frontend/image/favicon.png')}}">
    <title>W3Codex || News</title>
</head>
<body onload="startTime()">
<!--Load Page-->
<div class="load-page">
    <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
</div>
<!-- Mobile nav -->
<nav class="visible-sm visible-xs mobile-menu-container mobile-nav">
    <div class="menu-mobile-nav navbar-toggle">
        <span class="icon-search-mobile"><i class="fa fa-search" aria-hidden="true"></i></span>
        <span class="icon-bar"><i class="fa fa-bars" aria-hidden="true"></i></span>
    </div>
    <div id="cssmenu" class="animated">
        <div class="uni-icons-close"><i class="fa fa-times" aria-hidden="true"></i></div>
        <ul class="nav navbar-nav animated">
            <li class="has-sub home-icon"><a href='#'>Home</a></li>
        </ul>
        <div class="uni-nav-mobile-bottom">
            <div class="form-search-wrapper-mobile">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-addon success"><i class="fa fa-search"> search</i></span>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</nav>
<!-- End mobile menu -->


<div id="wrapper-container" class="site-wrapper-container">
    <header>
        <div class="vk-header-default">
            <div class="container-fluid">
                <div class="row">
                    <div class="vk-top-header">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="vk-top-header-1">
                                        <ul>
                                            <li><a href="#"> Contact</a></li>
                                            <li><a href="#">Purchase Now</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="vk-top-header-2">
                                        <ul>
                                            <li><span id="datetime-current"></span></li>
                                            <li>-</li>
                                            <li><span id="year-current"></span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="vk-top-header-3">
                                        <ul>
                                            <li><a href="{{ asset('/user/login') }}"> Login</a></li>
                                            <li><a href="{{ asset('/user/register') }}"> Register</a></li>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="vk-between-header">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="vk-between-header-logo">
                                        <a href="{{ asset('/') }}"><img src="{{asset('frontend/image/logo_24.png')}}" alt="" class="img-responsive"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-1">
                                    <div class="vk-between-header-banner">
                                        <a href="#"><img src="{{asset('frontend/image/ad-header.jpg')}}" alt="" class="img-responsive"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="visible-md visible-lg vk-bottom-header uni-sticky">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="cssmenu">
                                        <ul>
                                            <li class="has-sub home-icon">
                                                <a href='{{ asset('/') }}'><i class="fa fa-home" aria-hidden="true"></i></a>
                                            </li>
                                            @foreach($category as $cat)
                                            <li class="has-sub"><a href="{{asset('user/'. $cat->id)}}">{{ $cat->catName }}</a>
                                                <ul>
                                                    @foreach(explode(',',$cat->sub_category) as $subCat)
                                                        @if(!empty($subCat))
                                                            <li><a href="#">{{ $subCat }}</a></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>

                                           @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="vk-bottom-header-search toggle-form">
                                        {{--<i class="fa fa-search" aria-hidden="true"> search</i>--}}
                                        <form action="{{('/search')}}" method="post" enctype = "multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="input-group" >
                                                <input type="text" class="form-control" name="title" placeholder="Search for...">
                                                {{--<input type="hidden" class="form-control" name="id" placeholder="Search for...">--}}
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit" >
                                                        <a href="{{asset('/search')}}"><i class="fa fa-search"  style="color: red;"></i></a>
                                                    </button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!--Form search-->
                            {{--<div class="form-search-wrapper">
                                <form action="" method="post" >
                                    {{ csrf_field() }}
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search" placeholder="Search">
                                        <span class="input-group-addon success"><i class="fa fa-search"></i></span>
                                    </div>
                                </form>
                            </div>--}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="main-content" class="site-main-content">
        @section('content')
        @show
    </div>

    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="vk-sec-footer">
                    <div class="container">
                        <div class="row">
                            <div class="vk-footer">
                                <div class="footer-main-content-element col-sm-4">
                                    <aside class="widget">
                                        <div class="widget-title">
                                            <a href="{{ asset('/') }}"><img src="{{asset('frontend/image/logo_2.png')}}" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="widget-content">
                                            <div class="vk-footer-1">

                                                <div class="vk-footer-1-content">
                                                    <p>
                                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
                                                        egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.
                                                        Donec eu libero sit amet quam egestas semper.
                                                    </p>
                                                    <div class="vk-footer-1-address">
                                                        <ul>
                                                            <li><i class="fa fa-map-marker" aria-hidden="true"></i> <span>45 Queen's Park Rd, Brighton, BN2 0GJ, UK</span></li>
                                                            <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <span><a href="#">maxnews@domain.com</a></span></li>
                                                            <li><i class="fa fa-headphones" aria-hidden="true"></i> <span> (0123) 456 789</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="vk-footer-1-icon">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                                <div class="footer-main-content-element col-sm-4">
                                    <aside class="widget">
                                        <h3 class="widget-title"> Latest News</h3>
                                        <div class="widget-content">
                                            <div class="vk-footer-2">
                                                <div class="vk-footer-2-content">
                                                    <ul>
                                                        @php $i = 1; @endphp
                                                        @foreach($latest as $news)
                                                            <li>
                                                                <div class="vk-footer-img">
                                                                    <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('uploads/' . $news->image)}}" alt="" class="img-responsive img-gen"></a>
                                                                </div>
                                                                <div class="vk-footer-content">
                                                                    <div class="vk-footer-title">
                                                                        <h2><a href="{{ asset('/user/view/'. $news->id) }}"> {{ substr($news->title,0,50) }}...</a></h2>
                                                                    </div>
                                                                    <div class="vk-footer-time">
                                                                        <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($news->created_at))  }}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </li>
                                                            @php $i++ ;
                                                                if ($i== 4){
                                                                        echo '</li> <div class="vk-footer-img">';
                                                                        break ;
                                                                    }
                                                            @endphp
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                                <div class="footer-main-content-element col-sm-4">
                                    <aside class="widget">
                                        <h3 class="widget-title">Twitter Feed</h3>
                                        <div class="widget-content">
                                            <div class="vk-footer-3">
                                                <div class="vk-footer-3-content">
                                                    <p>
                                                        <span><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> MaxNews</a></span>
                                                        Your body is like a race car!! You need to fuel it right to keep performing at your best!
                                                    </p>
                                                    <div class="time">about 5 days ago</div>

                                                    <p>
                                                        <span><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> MaxNews</a></span>
                                                        Your body is like a race car!! You need to fuel it right to keep performing at your best!
                                                    </p>
                                                    <div class="time">about 5 days ago</div>

                                                    <p>
                                                        <span><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> MaxNews</a></span>
                                                        Your body is like a race car!! You need to fuel it right to keep performing at your best!
                                                    </p>
                                                    <div class="time">about 5 days ago</div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="vk-sub-footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="vk-sub-footer-1">
                                        <p>
                                            <span>MaxNews</span>
                                            -  News & Magazine PSD Template. Design by
                                            <span>Univertheme</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-5 col-md-offset-2">
                                    <div class="vk-sub-footer-2">
                                        <ul>
                                            <li><a href="#">Disclaimer </a></li>
                                            <li><a href="#"> Privacy</a></li>
                                            <li><a href="#"> Advertisement</a></li>
                                            <li><a href="#">Contact us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{{asset('frontend/js/jquery-2.0.2.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.sticky.js')}}"></script>
<script src="{{asset('frontend/js/masonry.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/dist/owl.carousel.js')}}"></script>
<script src="{{asset('frontend/js/main.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap-hover-tabs.js')}}"></script>
</body>

<!-- Mirrored from html.univertheme.com/maxnews/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Mar 2018 03:51:59 GMT -->
</html>
