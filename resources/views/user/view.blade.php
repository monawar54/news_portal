
@extends('layouts.master')
@section('content')
    <div id="main-content" class="site-main-content">
        <div id="home-main-content" class="site-home-main-content">
            <div class="uni-image-post">
                <div id="uni-home-defaults-slide">
                    <div id="vk-owl-demo-singer-slider" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="uni-item-img-1"></div>
                            <!--                        <img src="image/slideshow/center.jpg" alt="">-->
                            <div class="vk-item-caption">
                                <div class="vk-item-caption-top">
                                    <div class="ribbon">
                                        <span class="border-ribbon"><img src="image/homepage1/icon/fire.png" alt="" class="img-responsive"></span>
                                    </div>
                                    <ul>
                                        <li><a href="#">LIFESTYLE</a></li>
                                        <li>-</li>
                                        <li>2134 views</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="vk-item-caption-text">
                                    <h2><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                </div>
                                <div class="vk-item-caption-time">
                                    <span class="time"> June 21, 2017</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="uni-item-img-2"></div>
                            <!--                        <img src="image/slideshow/right.jpg" alt="">-->
                            <div class="vk-item-caption">
                                <div class="vk-item-caption-top">
                                    <div class="ribbon">
                                        <span class="border-ribbon"><img src="image/homepage1/icon/fire.png" alt="" class="img-responsive"></span>
                                    </div>
                                    <ul>
                                        <li><a href="#">SPORT</a></li>
                                        <li>-</li>
                                        <li>2134 views</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="vk-item-caption-text">
                                    <h2><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                </div>
                                <div class="vk-item-caption-time">
                                    <span class="time"> June 21, 2017</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="uni-item-img-3"></div>
                            <!--                        <img src="image/slideshow/left.jpg" alt="">-->
                            <div class="vk-item-caption">
                                <div class="vk-item-caption-top">
                                    <div class="ribbon">
                                        <span class="border-ribbon"><img src="image/homepage1/icon/fire.png" alt="" class="img-responsive"></span>
                                    </div>
                                    <ul>
                                        <li><a href="#">TRAVEL</a></li>
                                        <li>-</li>
                                        <li>2134 views</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="vk-item-caption-text">
                                    <h2><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                </div>
                                <div class="vk-item-caption-time">
                                    <span class="time"> June 21, 2017</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uni-image-post-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="uni-image-post-left">
                                    <main id="main" class="site-main alignleft">
                                        <div class="page-content">
                                            <article class="post-92 post type-post has-post-thumbnail">
                                                <div class="content-inner">
                                                    <div class="entry-content">
                                                        <header class="entry-header">
                                                            <h1 class="entry-title">{{ $data->title }}</h1>
                                                        </header><!-- .entry-header -->

                                                        <div class="entry-meta">
                                                            <span class="author vcard"><a href="#" rel="author"><i class="fa fa-user" aria-hidden="true"></i> Jonny Deep</a></span>
                                                            <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y",strtotime($data->created_at))  }}</span>
                                                            <span class="meta-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                            <span class="comment-total">
                                                                <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i>2 Comments</a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="entry-top">
                                                        <div class="post-formats-wrapper">
                                                            <a class="post-image" href="#">
                                                                <img src="{{ asset('uploads/'. $data->image )}}" class="attachment-full size-full wp-post-image img-responsive" alt="" >
                                                            </a>
                                                        </div>
                                                    </div><!-- .entry-top -->
                                                    <div class="entry-share">
                                                        <span class="title">Share</span>
                                                        <span class="fb"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                                                        <span class="tw"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                                                        <span class="gp"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></span>
                                                        <span class="pt"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></span>
                                                    </div>
                                                    <hr>
                                                    <div class="entry-content">
                                                        <div class="entry-description">
                                                            <p>{{ $data->description }}</p>
                                                            <br>
                                                            <p>{{ $data->description }}</p>
                                                            <div class="uni-qoutes">
                                                                <blockquote class="quote-card blue-card">
                                                                    <div class="uni-qoutes-container">
                                                                        <p>{{ $data->title }}</p>

                                                                        <cite>
                                                                            - Albert Einstein
                                                                        </cite>
                                                                    </div>
                                                                </blockquote>
                                                            </div>
                                                            <div class="entry-share">
                                                                <span class="title">Share</span>
                                                                <span class="fb"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                                                                <span class="tw"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                                                                <span class="gp"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></span>
                                                                <span class="pt"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></span>
                                                            </div>
                                                            <hr>
                                                            <div class="uni-comment-latest">
                                                                <div class="thumbnail-img">
                                                                    <a href="03_01_02_right_sidebar.html">
                                                                        <img src="{{ asset('frontend/image/03_02_01_image_post/pagination/img4.jpg')}}" alt="" title="" class="img-responsive">
                                                                    </a>
                                                                </div>
                                                                <div class="content-comment">
                                                                    <div class="author">
                                                                        <span class="author-name">Jonny Deep</span>
                                                                    </div>
                                                                    <div class="message">
                                                                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                            Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero
                                                                            sit amet quam egestas semper.</p>
                                                                    </div>
                                                                    <div class="link"><a href="#">http://jonnydeep.com</a></div>
                                                                    <ul class="share list-inline">
                                                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                                                        <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- .content-inner -->
                                                </div>
                                                <div class="related-archive">
                                                    <div class="vk-home-section-sport">
                                                        <div class="tab-default tabbable-panel">
                                                            <div class="tabbable-line">
                                                                <div class="uni-sticker-label">
                                                                    <div class="label-info">
                                                                        <a href="#">
                                                                            <span><i class="fa fa-bookmark" aria-hidden="true"></i></span>
                                                                            <span class="label-title">You may also read</span>
                                                                        </a>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="tab-content">
                                                                    <div class="tab-pane active" id="tab_sec-3_1">
                                                                        <div id="image-post-relate" class="owl-carousel owl-theme">
                                                                            <div class="item">
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="vk-home-section-2-left">
                                                                                            <div class="vk-sec-2-left-img">
                                                                                                <a href="{{ asset('/user/view/'. $econewest->id) }}"><img src="{{asset('uploads/'.$econewest->image)}}" alt=""></a>
                                                                                            </div>
                                                                                            <div class="vk-sec-2-left-content">
                                                                                                <h2><a href="{{ asset('/user/view/'. $econewest->id) }}">{{ substr( $econewest->title,0,65)  }}...</a></h2>
                                                                                                <div class="vk-sec-2-left-time">
                                                                                                    <ul>
                                                                                                        <li><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y",strtotime($econewest->created_at))  }}</li>
                                                                                                        <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                                        <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="vk-sec-2-left-text">
                                                                                                    <p> {{ substr($econewest->description,0,200) }}... </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        @php $i = 1; @endphp
                                                                                        @foreach($economy as $economys)
                                                                                        <div class="vk-home-section-2-right">
                                                                                            <ul>
                                                                                                <li>
                                                                                                    <div class="vk-sec-2-eco-img">
                                                                                                        <a href="{{ asset('/user/view/'. $economys->id) }}"><img src="{{asset('uploads/'. $economys->image)}}" alt="" class="img-responsive"></a>
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <div class="vk-sec-2-content">
                                                                                                        <div class="vk-sec-2-title">
                                                                                                            <h2><a href="{{ asset('/user/view/'. $economys->id) }}">{{ substr( $economys->title,0,45)  }} ...</a></h2>
                                                                                                        </div>
                                                                                                        <div class="vk-sec-2-time">
                                                                                                            <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($economys->created_at))  }}</div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </li>
                                                                                            </ul>
                                                                                            <div class="clearfix"></div>
                                                                                        </div>
                                                                                            @php $i++;
                                                                    if ($i== 6 || $i== 11){
                                                                        echo '</div> <div class="vk-home-section-2-right">';
                                                                        break ;
                                                                    }
                                                                                            @endphp
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--.related-->
                                            </article><!-- #post-## -->
                                            <div id="comments" class="comments-area">
                                                <div class="list-comments">
                                                    <h3 class="comments-title"><i class="fa fa-comments" aria-hidden="true"></i> 2 Comments</h3>
                                                    <hr>
                                                    <ul class="comment-list">
                                                        <li class="comment even thread-even depth-1 description_comment">
                                                            <img alt="" src="{{ asset('frontend/image/03_02_01_image_post/pagination/img5.jpg')}}" class="avatar photo img-responsive">
                                                            <div class="content-comment">
                                                                <div class="author">
                                                                    <span class="author-name">Adam Henderson</span>
                                                                </div>
                                                                <div class="message">
                                                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante</p>
                                                                </div>
                                                                <div class="time">
                                                                    <span class="comment-extra-info">October 6, 2016 at 9:59 am</span>
                                                                    <span>
                                                                <a rel="nofollow" class="comment-reply-link" href="#">Reply <i class="fa fa-share" aria-hidden="true"></i></a>
                                                            </span>
                                                                </div>
                                                            </div>

                                                            <div class="clear"></div>

                                                            <ul class="children">
                                                                <li class="comment odd alt depth-2 description_comment" id="comment-5">
                                                                    <img alt="" src="{{ asset('frontend/image/03_02_01_image_post/pagination/img6.jpg')}}" class="avatar avatar-100 photo">
                                                                    <div class="content-comment">
                                                                        <div class="author">
                                                                            <span class="author-name">Adam Henderson</span>
                                                                        </div>
                                                                        <div class="message">
                                                                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante</p>
                                                                        </div>
                                                                        <div class="time">
                                                                            <span class="comment-extra-info">October 6, 2016 at 9:59 am</span>
                                                                            <span>
                                                                <a rel="nofollow" class="comment-reply-link" href="#">Reply <i class="fa fa-share" aria-hidden="true"></i></a>
                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </li><!-- #comment-## -->
                                                            </ul><!-- .children -->
                                                        </li><!-- #comment-## -->
                                                    </ul>
                                                </div>

                                                <div class="form-comment">
                                                    <div id="respond" class="comment-respond">
                                                        <h3 id="reply-title" class="comment-reply-title"><i class="fa fa-pencil" aria-hidden="true"></i> Leave a Reply
                                                        </h3>
                                                        <hr>
                                                        <form action="http://html.univertheme.com/maxnews/action.html" method="post" id="commentform" class="comment-form">
                                                            <div class="row">
                                                                <div class="comment-form-comment form-group col-md-12">
                                                                    <textarea placeholder="Comment..." id="comment" name="comment" class="form-control"></textarea>
                                                                </div>
                                                                <div class="comment-form-author form-group col-md-4">
                                                                    <input placeholder="Name *" id="author" name="author" type="text" value="" class="form-control">
                                                                </div>
                                                                <div class="comment-form-email form-group col-md-4">
                                                                    <input placeholder="Email *" id="email" name="email" type="text" value="" class="form-control">
                                                                </div>
                                                                <div class="comment-form-url form-group col-md-4">
                                                                    <input placeholder="Website" id="url" name="url" type="text" value="" class="form-control">
                                                                </div>
                                                                <div class="form-submit form-group col-md-12">
                                                                    <input name="submit" type="submit" id="submit" class="submit" value="Post Comment" >
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div><!-- #respond -->
                                                </div>
                                            </div><!-- #comments -->
                                        </div><!-- .page-conten t-->
                                    </main>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="uni-image-post-right">
                                    <aside class="widget-area">

                                        <aside class="widget-area">

                                            <aside class="widget">
                                                <div class="widget-content">
                                                    <div class="vk-home-default-right-ad">
                                                        <a href="#">
                                                            <img src="{{asset('frontend/image/ad-sidebar.jpg')}}" alt="ad-sidebar" class="img-responsive">
                                                        </a>
                                                    </div>
                                                </div>
                                            </aside>

                                            <aside class="widget">
                                                <h3 class="widget-title">Editor Picks</h3>
                                                <div class="widget-content">
                                                    <div class="vk-home-default-right-ep">
                                                        <div id="vk-owl-ep-slider" class="owl-carousel owl-theme">
                                                            <div class="item">
                                                                @php $i = 1; @endphp
                                                                @if(count($pics)>0)
                                                                    @foreach($pics as $news)
                                                                <ul>
                                                                    <li>
                                                                        <div class="vk-item-ep">
                                                                            <div class="vk-item-ep-img">
                                                                                <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('uploads/' . $news->image)}}" alt="img"></a>
                                                                            </div>
                                                                            <div class="vk-item-ep-text">
                                                                                <h2><a href="{{ asset('/user/view/'. $news->id) }}">{{ substr( $news->title,0,45)  }}...</a></h2>
                                                                                <div class="vk-item-ep-time">
                                                                                    <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($news->created_at))  }}</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </li>
                                                                </ul>
                                                                        @php $i++;
                                                                  if($i == 6 || $i == 11) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                        @endphp
                                                                    @endforeach
                                                                @else
                                                                    {{ 'News not Found' }}
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </aside>

                                            <aside class="widget">
                                                <div class="widget-content">
                                                    <div class="vk-home-default-right-facebook">
                                                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&amp;tabs=timeline&amp;width=340&amp;height=500&amp;small_header=false&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId" width="340" height="500" style="border:none;overflow:hidden"></iframe>
                                                    </div>
                                                </div>
                                            </aside>

                                            <aside class="widget">
                                                <h3 class="widget-title">Feature Videos</h3>
                                                <div class="widget-content">
                                                    <div class="vk-home-default-right-fea">
                                                        <div id="vk-owl-fea-slider" class="owl-carousel owl-theme">
                                                            <div class="item">
                                                                @php $i = 1; @endphp
                                                                @if(count($video)>0)
                                                                    @foreach($video as $news)
                                                                <ul>
                                                                    <li>
                                                                        <div class="vk-item-fea">
                                                                            <div class="uni-play-img vk-item-fea-img">
                                                                                <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/v-Dur3uXXCQ" frameborder="0" allowfullscreen></iframe>-->
                                                                                <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('uploads/' . $news->image)}}" alt="img-2"></a>
                                                                                <div class="uni-play">
                                                                                    <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('frontend/image/play.png')}}" alt="" class="img-responsive"></a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="vk-item-fea-text">
                                                                                <h2><a href="{{ asset('/user/view/'. $news->id) }}">{{ substr( $news->title,0,75)  }}...</a></h2>
                                                                                <div class="vk-item-fea-time">
                                                                                    <ul>
                                                                                        <li><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($news->created_at))  }}</li>
                                                                                        <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                        <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </li>
                                                                </ul>
                                                                        @php $i++;
                                                                  if($i == 4 || $i == 7) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                        @endphp
                                                                    @endforeach
                                                                @else
                                                                    {{ 'News not Found' }}
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </aside>

                                            <aside class="widget">
                                                <h3 class="widget-title">Stay Connected</h3>
                                                <div class="widget-content">
                                                    <div class="vk-home-default-right-stay">
                                                        <div class="vk-right-stay-body">
                                                            <a class="btn btn-block btn-social btn-facebook">
                                                                <span class="icon"><i class="fa fa-facebook"></i></span>
                                                                <span class="info"> 2134 Like</span>
                                                                <span class="text">Like</span>
                                                            </a>
                                                            <a class="btn btn-block btn-social btn-twitter">
                                                                <span class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                                <span class="info"> 13634 Follows</span>
                                                                <span class="text">Follows</span>
                                                            </a>
                                                            <a class="btn btn-block btn-social btn-youtube">
                                                                <span class="icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
                                                                <span class="info">10634 Subscribers</span>
                                                                <span class="text">Subscribe</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </aside>

                                            <aside class="widget">
                                                <h3 class="widget-title">Tags Clound</h3>
                                                <div class="widget-content">
                                                    <div class="vk-home-default-right-tags">
                                                        <ul>
                                                            <li><a href="#">LifeStyle</a></li>
                                                            <li><a href="#">Sport</a></li>
                                                            <li><a href="#">Economy</a></li>
                                                            <li><a href="#">Business</a></li>
                                                            <li><a href="#">Travel</a></li>
                                                            <li><a href="#">Techology</a></li>
                                                            <li><a href="#">Movies</a></li>
                                                            <li><a href="#">Fashion</a></li>
                                                            <li><a href="#">video</a></li>
                                                            <li><a href="#">Music</a></li>
                                                            <li><a href="#">Photography</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </aside>
                                        </aside>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection