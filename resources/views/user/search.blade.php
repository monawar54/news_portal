@extends('layouts.master')
@section('content')
    <div id="main-content" class="site-main-content">
        <div id="home-main-content" class="site-home-main-content">
            <div class="uni-search-result-list">
                <div id="uni-home-defaults-slide">
                    <div id="vk-owl-demo-singer-slider" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="uni-item-img-1"></div>
                            <!--                        <img src="image/slideshow/center.jpg" alt="">-->
                            <div class="vk-item-caption">
                                <div class="vk-item-caption-top">
                                    <div class="ribbon">
                                        <span class="border-ribbon"><img src="image/homepage1/icon/fire.png" alt="" class="img-responsive"></span>
                                    </div>
                                    <ul>
                                        <li><a href="#">LIFESTYLE</a></li>
                                        <li>-</li>
                                        <li>2134 views</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="vk-item-caption-text">
                                    <h2><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                </div>
                                <div class="vk-item-caption-time">
                                    <span class="time"> June 21, 2017</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="uni-item-img-2"></div>
                            <!--                        <img src="image/slideshow/right.jpg" alt="">-->
                            <div class="vk-item-caption">
                                <div class="vk-item-caption-top">
                                    <div class="ribbon">
                                        <span class="border-ribbon"><img src="image/homepage1/icon/fire.png" alt="" class="img-responsive"></span>
                                    </div>
                                    <ul>
                                        <li><a href="#">SPORT</a></li>
                                        <li>-</li>
                                        <li>2134 views</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="vk-item-caption-text">
                                    <h2><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                </div>
                                <div class="vk-item-caption-time">
                                    <span class="time"> June 21, 2017</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="uni-item-img-3"></div>
                            <!--                        <img src="image/slideshow/left.jpg" alt="">-->
                            <div class="vk-item-caption">
                                <div class="vk-item-caption-top">
                                    <div class="ribbon">
                                        <span class="border-ribbon"><img src="image/homepage1/icon/fire.png" alt="" class="img-responsive"></span>
                                    </div>
                                    <ul>
                                        <li><a href="#">TRAVEL</a></li>
                                        <li>-</li>
                                        <li>2134 views</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="vk-item-caption-text">
                                    <h2><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                </div>
                                <div class="vk-item-caption-time">
                                    <span class="time"> June 21, 2017</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uni-search-result-list-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="uni-search-result-list-left">
                                    <main id="main" class="site-main alignleft">

                                        <div class="breadcrumb breadcrumb-search">
                                            <a href="#">Home</a>
                                            <a href="#" class="active">Search</a>
                                        </div>

                                        <div class="uni-search-title">
                                            <h3>126 search results for <span>Football</span></h3>
                                            <div class="uni-search-icons">
                                                <span><a href="03_03_01_search_results_list.html" class="active"><i class="fa fa-list" aria-hidden="true"></i></a></span>
                                                <span><a href="03_03_02_search_results_grid.html"><i class="fa fa-th" aria-hidden="true"></i></a></span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <hr>

                                        <div class="uni-search-item-body">
                                            <div class="row">
                                                <div class="col-md-6">

                                                </div>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="{{asset('frontend/image/02_01_list/img-1.jpg')}}" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel=""> habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                                    <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                                </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-1.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-2.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-3.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-4.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-5.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-6.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-7.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-8.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img2.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img3.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img4.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img4-1.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img3-1.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="uni-item">
                                                <article  class="post type-post">
                                                    <div class="content-inner">
                                                        <div class="entry-top">
                                                            <div class="thumbnail-img"><a href="03_02_01_image_post.html" title=""><img src="image/02_01_list/img-1.jpg" alt="" title="" class="img-responsive"></a>
                                                            </div>
                                                        </div>

                                                        <div class="entry-content">
                                                            <header class="entry-header">
                                                                <h2 class="entry-title">
                                                                    <a href="03_02_01_image_post.html" rel="">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas
                                                                    </a>
                                                                </h2>
                                                            </header>

                                                            <div class="entry-meta">
                                                                <span class="entry-date"><i class="fa fa-calendar" aria-hidden="true"></i> June 21, 2017</span>
                                                                <span class="entry-views"><i class="fa fa-eye" aria-hidden="true"></i> 2134 views</span>
                                                                <span class="comment-total">
                                                        <a href="#" class="comments-link"><i class="fa fa-comments" aria-hidden="true"></i> 30 Comments</a>
                                                    </span>
                                                            </div>
                                                            <div class="entry-summary">
                                                                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                                    Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        </div>

                                        <ul class="loop-pagination">
                                            <li class="current"><a class="page-numbers">1</a></li>
                                            <li><a class="page-numbers" href="#">2</a></li>
                                            <li><a class="page-numbers" href="#">3</a></li>
                                            <li><a class="page-numbers" href="#">...</a></li>
                                            <li><a class="page-numbers" href="#">9</a></li>
                                            <li><a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul><!-- .pagination -->
                                    </main>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="uni-search-result-list-right">
                                    <aside class="widget-area">

                                        <aside class="widget">
                                            <div class="widget-content">
                                                <div class="vk-home-default-right-ad">
                                                    <a href="#">
                                                        <img src="{{asset('frontend/image/ad-sidebar.jpg')}}" alt="ad-sidebar" class="img-responsive">
                                                    </a>
                                                </div>
                                            </div>
                                        </aside>

                                        <aside class="widget">
                                            <h3 class="widget-title">Editor Picks</h3>
                                            <div class="widget-content">
                                                <div class="vk-home-default-right-ep">
                                                    <div id="vk-owl-ep-slider" class="owl-carousel owl-theme">
                                                        <div class="item">
                                                            <ul>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img.jpg" alt="img"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-1.jpg" alt="img-1"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-2.jpg" alt="img-2"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-3.jpg" alt="img-3"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-4.jpg" alt="img-4"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="item">
                                                            <ul>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img.jpg" alt="img"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-1.jpg" alt="img-1"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-2.jpg" alt="img-2"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-3.jpg" alt="img-3"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-4.jpg" alt="img-4"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="item">
                                                            <ul>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img.jpg" alt="img"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-1.jpg" alt="img-1"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-2.jpg" alt="img-2"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-3.jpg" alt="img-3"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-ep">
                                                                        <div class="vk-item-ep-img">
                                                                            <a href="#"><img src="image/siderbar/img-4.jpg" alt="ìm-4"></a>
                                                                        </div>
                                                                        <div class="vk-item-ep-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-ep-time">
                                                                                <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>

                                        <aside class="widget">
                                            <div class="widget-content">
                                                <div class="vk-home-default-right-facebook">
                                                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&amp;tabs=timeline&amp;width=340&amp;height=500&amp;small_header=false&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId" width="340" height="500" style="border:none;overflow:hidden"></iframe>
                                                </div>
                                            </div>
                                        </aside>

                                        <aside class="widget">
                                            <h3 class="widget-title">Feature Videos</h3>
                                            <div class="widget-content">
                                                <div class="vk-home-default-right-fea">
                                                    <div id="vk-owl-fea-slider" class="owl-carousel owl-theme">
                                                        <div class="item">
                                                            <ul>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/v-Dur3uXXCQ" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="03_02_03_video_post.html"><img src="image/slideshow/img-2.jpg" alt="img-2"></a>
                                                                            <div class="uni-play">
                                                                                <a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="03_02_03_video_post.html">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/WyiIGEHQP8o" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="03_02_03_video_post.html"><img src="image/slideshow/img-1.jpg" alt="img-1"></a>
                                                                            <div class="uni-play"><a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a></div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="#">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/zEkg4GBQumc" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="03_02_03_video_post.html"><img src="image/slideshow/img.jpg" alt="img"></a>
                                                                            <div class="uni-play">
                                                                                <a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="03_02_03_video_post.html">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="item">
                                                            <ul>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/v-Dur3uXXCQ" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="03_02_03_video_post.html"><img src="image/slideshow/img-2.jpg" alt="img-2"></a>
                                                                            <div class="uni-play">
                                                                                <a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="03_02_03_video_post.html">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/WyiIGEHQP8o" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="03_02_03_video_post.html"><img src="image/slideshow/img-1.jpg" alt="img-1"></a>
                                                                            <div class="uni-play">
                                                                                <a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="03_02_03_video_post.html">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/zEkg4GBQumc" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="03_02_03_video_post.html"><img src="image/slideshow/img.jpg" alt="img"></a>
                                                                            <div class="uni-play">
                                                                                <a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="03_02_03_video_post.html">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="item">
                                                            <ul>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/v-Dur3uXXCQ" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="03_02_03_video_post.html"><img src="image/slideshow/img-2.jpg" alt="img-2"></a>
                                                                            <div class="uni-play">
                                                                                <a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="03_02_03_video_post.html">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/WyiIGEHQP8o" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="#"><img src="image/slideshow/img-1.jpg" alt="img-1"></a>
                                                                            <div class="uni-play">
                                                                                <a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="03_02_03_video_post.html">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                                <li>
                                                                    <div class="vk-item-fea">
                                                                        <div class="uni-play-img vk-item-fea-img">
                                                                            <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/zEkg4GBQumc" frameborder="0" allowfullscreen></iframe>-->
                                                                            <a href="03_02_03_video_post.html"><img src="image/slideshow/img.jpg" alt="img"></a>
                                                                            <div class="uni-play">
                                                                                <a href="03_02_03_video_post.html"><img src="image/play.png" alt="" class="img-responsive"></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="vk-item-fea-text">
                                                                            <h2><a href="03_02_03_video_post.html">Pellentesque habitant morbi tristi senectus et netus</a></h2>
                                                                            <div class="vk-item-fea-time">
                                                                                <ul>
                                                                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</li>
                                                                                    <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                    <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>

                                        <aside class="widget">
                                            <h3 class="widget-title">Stay Connected</h3>
                                            <div class="widget-content">
                                                <div class="vk-home-default-right-stay">
                                                    <div class="vk-right-stay-body">
                                                        <a class="btn btn-block btn-social btn-facebook">
                                                            <span class="icon"><i class="fa fa-facebook"></i></span>
                                                            <span class="info"> 2134 Like</span>
                                                            <span class="text">Like</span>
                                                        </a>
                                                        <a class="btn btn-block btn-social btn-twitter">
                                                            <span class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                            <span class="info"> 13634 Follows</span>
                                                            <span class="text">Follows</span>
                                                        </a>
                                                        <a class="btn btn-block btn-social btn-youtube">
                                                            <span class="icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
                                                            <span class="info">10634 Subscribers</span>
                                                            <span class="text">Subscribe</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>

                                        <aside class="widget">
                                            <div class="widget-content">
                                                <div class="vk-home-default-right-ad">
                                                    <a href="#">
                                                        <img src="image/ad-sidebar.jpg" alt="ad-sidebar" class="img-responsive">
                                                    </a>
                                                </div>
                                            </div>
                                        </aside>

                                        <aside class="widget">
                                            <h3 class="widget-title">Tags Clound</h3>
                                            <div class="widget-content">
                                                <div class="vk-home-default-right-tags">
                                                    <ul>
                                                        <li><a href="#">LifeStyle</a></li>
                                                        <li><a href="#">Sport</a></li>
                                                        <li><a href="#">Economy</a></li>
                                                        <li><a href="#">Business</a></li>
                                                        <li><a href="#">Travel</a></li>
                                                        <li><a href="#">Techology</a></li>
                                                        <li><a href="#">Movies</a></li>
                                                        <li><a href="#">Fashion</a></li>
                                                        <li><a href="#">video</a></li>
                                                        <li><a href="#">Music</a></li>
                                                        <li><a href="#">Photography</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </aside>

                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection