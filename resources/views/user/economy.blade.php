@extends('layouts.master')
@section('content')

    <div id="vk-home-techology">
        <div id="wrapper-container" class="site-wrapper-container">
            <div id="main-content" class="site-main-content">
                <div id="home-main-content" class="site-home-main-content">
                    <div class="vk-home-techolohy-body">
                        <div class="container">
                            <div class="vk-home-tech-art-aside">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="vk-home-tech-left">
                                            <div class="vk-tech-left-sec-2">
                                                <div class="clearfix"></div>
                                                <div class="vk-home-tech-sec-4">
                                                    <div class="uni-sticker-label">
                                                        <div class="label-info">
                                                            <a href="#">
                                                                <img src="{{asset('frontend/image/homepage1/icon/fire.png')}}" alt="" class="img-responsive img-gen">
                                                                <img src="{{asset('frontend/image/fire-red.png')}}" alt="" class="img-responsive img-hover">
                                                                <span class="label-title">Economy  News</span>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        @php $i = 1; @endphp
                                                        @foreach($category_news as $economies)
                                                            <div class="col-md-6">
                                                                <div class="vk-fashion-sec-2-item">

                                                                    <div class="vk-fashion-sec-2-left-img">
                                                                        <a href="{{ asset('/user/view/'. $economies->id) }}"><img src="{{asset('uploads/'. $economies->image)}}" alt="" class="img-responsive"></a>
                                                                    </div>
                                                                    <div class="vk-fashion-sec-2-left-content">
                                                                        <h2><a href="{{ asset('/user/view/'. $economies->id) }}"> {{ substr( $economies->title,0,85)  }} ...</a></h2>
                                                                        <div class="vk-fashion-sec-2-left-time">
                                                                            <ul>
                                                                                <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($economies->created_at))  }}</a></li>
                                                                                <li><a href="#"> <i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</a></li>
                                                                                <li><a href="#"> <i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</a></li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="vk-fashion-sec-2-left-text">
                                                                            <p> {{ substr($economies->description,0,200) }}... </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vk-fashion-sec-2-left-btn">
                                                                        <a href="{{ asset('/user/view/'. $economies->id) }}"> Read more <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @php $i++; @endphp
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <ul class="loop-pagination">
                                                    <li class="current"><a class="page-numbers">1</a></li>
                                                    <li><a class="page-numbers" href="#">2</a></li>
                                                    <li><a class="page-numbers" href="#">3</a></li>
                                                    <li><a class="page-numbers" href="#">...</a></li>
                                                    <li><a class="page-numbers" href="#">9</a></li>
                                                    <li><a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="vk-home-tech-right">
                                            <aside class="widget-area">
                                                <aside class="widget">
                                                    <div class="widget-content">
                                                        <div class="vk-home-default-right-ad">
                                                            <a href="#">
                                                                <img src="{{asset('frontend/image/ad-sidebar.jpg')}}" alt="ad-sidebar" class="img-responsive">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </aside>

                                                <aside class="widget">
                                                    <h3 class="widget-title">Editor Picks</h3>
                                                    <div class="widget-content">
                                                        <div class="vk-home-default-right-ep">
                                                            <div id="vk-owl-ep-slider" class="owl-carousel owl-theme">
                                                                <div class="item">
                                                                    @php $i = 1; @endphp
                                                                    @if(count($pics)>0)
                                                                        @foreach($pics as $news)
                                                                    <ul>
                                                                        <li>
                                                                            <div class="vk-item-ep">
                                                                                <div class="vk-item-ep-img">
                                                                                    <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('uploads/' . $news->image)}}" alt="img"></a>
                                                                                </div>
                                                                                <div class="vk-item-ep-text">
                                                                                    <h2><a href="{{ asset('/user/view/'. $news->id) }}">{{ substr( $news->title,0,45)  }}...</a></h2>
                                                                                    <div class="vk-item-ep-time">
                                                                                        <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($news->created_at))  }}</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </li>
                                                                    </ul>
                                                                            @php $i++;
                                                                  if($i == 6 || $i == 11) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                            @endphp
                                                                        @endforeach
                                                                    @else
                                                                        {{ 'News not Found' }}
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </aside>

                                                <aside class="widget">
                                                    <div class="widget-content">
                                                        <div class="vk-home-default-right-facebook">
                                                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&amp;tabs=timeline&amp;width=340&amp;height=500&amp;small_header=false&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId" width="340" height="500" style="border:none;overflow:hidden"></iframe>
                                                        </div>
                                                    </div>
                                                </aside>

                                                <aside class="widget">
                                                    <h3 class="widget-title">Feature Videos</h3>
                                                    <div class="widget-content">
                                                        <div class="vk-home-default-right-fea">
                                                            <div id="vk-owl-fea-slider" class="owl-carousel owl-theme">
                                                                <div class="item">
                                                                    @php $i = 1; @endphp
                                                                    @if(count($video)>0)
                                                                        @foreach($video as $news)
                                                                    <ul>
                                                                        <li>
                                                                            <div class="vk-item-fea">
                                                                                <div class="uni-play-img vk-item-fea-img">
                                                                                    <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/v-Dur3uXXCQ" frameborder="0" allowfullscreen></iframe>-->
                                                                                    <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('uploads/' . $news->image)}}" alt="img-2"></a>
                                                                                    <div class="uni-play">
                                                                                        <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('frontend/image/play.png')}}" alt="" class="img-responsive"></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="vk-item-fea-text">
                                                                                    <h2><a href="{{ asset('/user/view/'. $news->id) }}"> {{ substr( $news->title,0,75)  }}...</a></h2>
                                                                                    <div class="vk-item-fea-time">
                                                                                        <ul>
                                                                                            <li><i class="fa fa-calendar" aria-hidden="true"></i>{{ date("F j, Y",strtotime($news->created_at))  }}</li>
                                                                                            <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                            <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </li>
                                                                    </ul>
                                                                            @php $i++;
                                                                  if($i == 4 || $i == 7) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                            @endphp
                                                                        @endforeach
                                                                    @else
                                                                        {{ 'News not Found' }}
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </aside>

                                                <aside class="widget">
                                                    <h3 class="widget-title">Stay Connected</h3>
                                                    <div class="widget-content">
                                                        <div class="vk-home-default-right-stay">
                                                            <div class="vk-right-stay-body">
                                                                <a class="btn btn-block btn-social btn-facebook">
                                                                    <span class="icon"><i class="fa fa-facebook"></i></span>
                                                                    <span class="info"> 2134 Like</span>
                                                                    <span class="text">Like</span>
                                                                </a>
                                                                <a class="btn btn-block btn-social btn-twitter">
                                                                    <span class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                                    <span class="info"> 13634 Follows</span>
                                                                    <span class="text">Follows</span>
                                                                </a>
                                                                <a class="btn btn-block btn-social btn-youtube">
                                                                    <span class="icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
                                                                    <span class="info">10634 Subscribers</span>
                                                                    <span class="text">Subscribe</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </aside>

                                                <aside class="widget">
                                                    <div class="widget-content">
                                                        <div class="vk-home-default-right-ad">
                                                            <a href="#">
                                                                <img src="image/ad-sidebar.jpg" alt="ad-sidebar" class="img-responsive">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </aside>

                                                <aside class="widget">
                                                    <h3 class="widget-title">Tags Clound</h3>
                                                    <div class="widget-content">
                                                        <div class="vk-home-default-right-tags">
                                                            <ul>
                                                                <li><a href="#">LifeStyle</a></li>
                                                                <li><a href="#">Sport</a></li>
                                                                <li><a href="#">Economy</a></li>
                                                                <li><a href="#">Business</a></li>
                                                                <li><a href="#">Travel</a></li>
                                                                <li><a href="#">Techology</a></li>
                                                                <li><a href="#">Movies</a></li>
                                                                <li><a href="#">Fashion</a></li>
                                                                <li><a href="#">video</a></li>
                                                                <li><a href="#">Music</a></li>
                                                                <li><a href="#">Photography</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </aside>
                                            </aside>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
