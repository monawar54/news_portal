@extends('layouts.master')
@section('content')

    <div id="home-main-content" class="site-home-main-content">
        <div id="vk-home-default-slide">
            <div class="container-fluid">
                <div class="row">
                    <!--HOME 1 SLIDE-->
                    <div id="uni-home-defaults-slide">
                        <div id="vk-owl-demo-singer-slider" class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="uni-item-img-1"></div>
                                <!--                        <img src="image/slideshow/center.jpg" alt="">-->
                                <div class="vk-item-caption">
                                    <div class="vk-item-caption-top">
                                        <div class="ribbon">
                                            <span class="border-ribbon"><img src="{{asset('frontend/image/homepage1/icon/fire.png')}}" alt="" class="img-responsive"></span>
                                        </div>
                                        <ul>
                                            <li><a href="#">LIFESTYLE</a></li>
                                            <li>-</li>
                                            <li>2134 views</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="vk-item-caption-text">
                                        <h2><a href="03_02_01_image_post.html">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                    </div>
                                    <div class="vk-item-caption-time">
                                        <span class="time"> June 21, 2017</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="uni-item-img-2"></div>
                            <!--                        <img src="{{asset('frontend/image/slideshow/right.jpg')}}" alt="">-->
                                <div class="vk-item-caption">
                                    <div class="vk-item-caption-top">
                                        <div class="ribbon">
                                            <span class="border-ribbon"><img src="{{asset('frontend/image/homepage1/icon/fire.png')}}" alt="" class="img-responsive"></span>
                                        </div>
                                        <ul>
                                            <li><a href="#">SPORT</a></li>
                                            <li>-</li>
                                            <li>2134 views</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="vk-item-caption-text">
                                        <h2><a href="03_02_01_image_post.html">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                    </div>
                                    <div class="vk-item-caption-time">
                                        <span class="time"> June 21, 2017</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="uni-item-img-3"></div>
                            <!--                        <img src="{{asset('frontend/image/slideshow/left.jpg')}}" alt="">-->
                                <div class="vk-item-caption">
                                    <div class="vk-item-caption-top">
                                        <div class="ribbon">
                                            <span class="border-ribbon"><img src="{{asset('frontend/image/homepage1/icon/fire.png')}}" alt="" class="img-responsive"></span>
                                        </div>
                                        <ul>
                                            <li><a href="#">TRAVEL</a></li>
                                            <li>-</li>
                                            <li>2134 views</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="vk-item-caption-text">
                                        <h2><a href="03_02_01_image_post.html">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h2>
                                    </div>
                                    <div class="vk-item-caption-time">
                                        <span class="time"> June 21, 2017</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="vk-home-default-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div id="vk-home-default-left">

                            <!--HOME DEFAULT HOT NEWS-->
                            <div class="vk-home-section-hotnews">
                                <div class="tab-default tabbable-panel">
                                    <div class="tabbable-line">
                                        <div class="uni-sticker-label">
                                            <div class="label-info">
                                                <a href="#">
                                                    {{--<img src="image/homepage1/icon/data.png" alt="" class="img-responsive img-gen">--}}
                                                    <img src="{{asset('frontend/image/homepage1/icon/fire.png')}}" alt="" class="img-responsive img-gen">
                                                    <img src="{{asset('frontend/image/fire-red.png')}}" alt="" class="img-responsive img-hover">
                                                    <span class="label-title">Hot News</span>
                                                </a>
                                            </div>
                                            <!--Tab show destop-->
                                            <ul class="visible-sm visible-md visible-lg nav nav-tabs ">
                                                <li class="active">
                                                    <a href="#tab_default_1" data-toggle="tab">Economy </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_default_2" data-toggle="tab">Sport </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_default_3" data-toggle="tab">Fashion </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_default_4" data-toggle="tab">Music </a>
                                                </li>
                                                <li>
                                                    <a href="#tab_default_5" data-toggle="tab">Video </a>
                                                </li>
                                            </ul>

                                            <!--Tab show mobiles-->
                                            <div class="btn btn-select btn-select-light">
                                                <input type="hidden" class="btn-select-input" name="hot-news-input" value="" />
                                                <span class="btn-select-value">Economy</span>
                                                <span class="btn-select-arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                                <ul>
                                                    <li><a href="#tab_default_1" data-toggle="tab">Economy </a></li>
                                                    <li><a href="#tab_default_2" data-toggle="tab">Sport </a></li>
                                                    <li><a href="#tab_default_3" data-toggle="tab">Fashion </a></li>
                                                    <li><a href="#tab_default_4" data-toggle="tab">Music </a></li>
                                                    <li><a href="#tab_default_5" data-toggle="tab">Video </a></li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_default_1">
                                                <div class="vk-section1-eco">
                                                    <div id="uni-home-default-hotnews-economy-slide" class="owl-carousel owl-theme">
                                                        <div class="item">
                                                            @php $i = 1; @endphp
                                                            @if(count($economy)>0)
                                                                @foreach($economy as $news)
                                                                    <div class="vk-section1-eco-item">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="vk-section1-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $news->id) }}">
                                                                                        <img width="370" height="270" src="{{asset('uploads/'.$news->image)}}" alt="" class="img-responsive">
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-section-content">
                                                                                    <div class="vk-section1-eco-title">
                                                                                        <h2>
                                                                                            <a href="{{ asset('/user/view/'. $news->id) }}">{{ substr($news->title,0,65) }}...</a>
                                                                                        </h2>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-time">
                                                                                        <ul>
                                                                                            <li><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y, g:i a",strtotime($news->created_at))  }}</li>
                                                                                            <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                            <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-text">
                                                                                        <p> {{ substr($news->description,0,200) }}... </p>
                                                                                    </div>
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                  if($i == 4 || $i ==7) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                {{ 'News not Found' }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_default_2">
                                                <div class="vk-section1-eco">
                                                    <div id="uni-home-default-hotnews-sport-slide" class="owl-carousel owl-theme">
                                                        <div class="item">

                                                            @php $i = 1; @endphp
                                                            @if(count($sports)>0)
                                                                @foreach($sports as $sport)

                                                                    <div class="vk-section1-eco-item">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="vk-section1-eco-img">

                                                                                    <a href="{{ asset('/user/view/'. $sport->id) }}"><img src="{{asset('uploads/'.$sport->image)}}" alt="" class="img-responsive"></a>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-section-content">
                                                                                    <div class="vk-section1-eco-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $sport->id) }}">{{ substr($sport->title,0,65) }}... </a></h2>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-time">
                                                                                        <ul>
                                                                                            <li><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y, g:i a",strtotime($news->created_at))  }}</li>
                                                                                            <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                            <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-text">
                                                                                        <p> {{ substr($sport->description,0,200) }}... </p>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                  if($i == 4 || $i ==7) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                {{ 'News not Found' }}
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_default_3">
                                                <div class="vk-section1-eco">
                                                    <div id="uni-home-default-hotnews-fashion-slide" class="owl-carousel owl-theme">
                                                        <div class="item">
                                                            @php $i = 1; @endphp
                                                            @if(count($fashions)>0)
                                                                @foreach($fashions as $fashion)
                                                                    <div class="vk-section1-eco-item">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="vk-section1-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $fashion->id) }}"><img src="{{asset('uploads/'.$fashion->image)}}" alt="" class="img-responsive"></a>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-section-content">
                                                                                    <div class="vk-section1-eco-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $fashion->id) }}">{{ substr($fashion->title,0,65) }}...</a></h2>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-time">
                                                                                        <ul>
                                                                                            <li><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y, g:i a",strtotime($news->created_at))  }}</li>
                                                                                            <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                            <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-text">
                                                                                        <p> {{ substr($fashion->description,0,200) }}... </p>
                                                                                    </div>
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                  if($i == 4 || $i ==7) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                {{ 'News not Found' }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_default_4">
                                                <div class="vk-section1-eco">
                                                    <div id="uni-home-default-hotnews-music-slide" class="owl-carousel owl-theme">
                                                        <div class="item">
                                                            @php $i = 1; @endphp
                                                            @if(count($music)>0)
                                                                @foreach($music as $musics)
                                                                    <div class="vk-section1-eco-item">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="vk-section1-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $musics->id) }}"><img src="{{asset('uploads/'.$musics->image)}}" alt="" class="img-responsive"></a>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-section-content">
                                                                                    <div class="vk-section1-eco-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $musics->id) }}">{{ substr($musics->title,0,65) }}...  </a></h2>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-time">
                                                                                        <ul>
                                                                                            <li><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y, g:i a",strtotime($news->created_at))  }}</li>
                                                                                            <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                            <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-text">
                                                                                        <p> {{ substr($musics->description,0,200) }}... </p>
                                                                                    </div>
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                  if($i == 4 || $i ==7) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                {{ 'News not Found' }}
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_default_5">
                                                <div class="vk-section1-eco">
                                                    <div id="uni-home-default-hotnews-video-slide" class="owl-carousel owl-theme">
                                                        <div class="item">

                                                            @php $i = 1; @endphp
                                                            @if(count($video)>0)
                                                                @foreach($video as $videos)

                                                                    <div class="vk-section1-eco-item">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="uni-play-img vk-section1-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $videos->id) }}"><img src="{{asset('uploads/'.$videos->image)}}" alt="" class="img-responsive"></a>
                                                                                    <div class="uni-play">
                                                                                        <a href="03_02_03_video_post.html"><img src="{{asset('frontend/image/play.png')}}" alt=""></a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-section-content">
                                                                                    <div class="vk-section1-eco-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $videos->id) }}">{{ substr($videos->title,0,65)  }}...</a></h2>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-time">
                                                                                        <ul>
                                                                                            <li><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y, g:i a",strtotime($news->created_at))  }}</li>
                                                                                            <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                            <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="vk-section1-eco-text">
                                                                                        <p> {{ substr($videos->description,0,200) }}... </p>
                                                                                    </div>
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                  if($i == 4 || $i ==7) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                                    @endphp
                                                                @endforeach
                                                            @else
                                                                {{ 'News not Found' }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--HOME DEFAULT ECONOMY-->
                            <div class="vk-home-section-economy">
                                <div class="tab-default tabbable-panel">
                                    <div class="tabbable-line">
                                        <div class="uni-sticker-label">
                                            <div class="label-info">
                                                <a href="{{asset('user/'. $econewest->id)}}">
                                                    <img src="{{ asset('frontend/image/homepage1/icon/data.png') }}" alt="" class="img-responsive img-gen">
                                                    <img src="{{asset('frontend/image/eco-red.png')}}" alt="" class="img-responsive img-hover">
                                                    <span class="label-title">Economy</span>
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_sec-2_1">
                                                <div id="uni-home-default-fashion-newest-slide" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="vk-home-section-2-left">
                                                                    <div class="vk-sec-2-left-img">

                                                                        <a href="{{ asset('/user/view/'. $econewest->id) }}"><img src="{{asset('uploads/'.$econewest->image)}}" alt=""></a>
                                                                    </div>
                                                                    <div class="vk-sec-2-left-content">
                                                                        <h2><a href="{{ asset('/user/view/'. $econewest->id) }}"> {{ substr( $econewest->title,0,65)  }}...</a></h2>
                                                                        <div class="vk-sec-2-left-time">
                                                                            <ul>
                                                                                <li><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($news->created_at))  }}</li>
                                                                                <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="vk-sec-2-left-text">
                                                                            <p> {{ substr($econewest->description,0,200) }}... </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                @php $i = 1; @endphp
                                                                @foreach($economy as $economys)
                                                                    <div class="vk-home-section-2-right">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="vk-sec-2-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $economys->id) }}"><img src="{{asset('uploads/'. $economys->image)}}" alt="" class="img-responsive"></a>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-sec-2-content">
                                                                                    <div class="vk-sec-2-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $economys->id) }}"> {{ substr( $economys->title,0,45)  }} ...</a></h2>
                                                                                    </div>
                                                                                    <div class="vk-sec-2-time">
                                                                                        <span class="time"><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y",strtotime($news->created_at))  }}</span>
                                                                                    </div>
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                    if ($i== 6 || $i== 11){
                                                                        echo '</div> <div class="vk-home-section-2-right">';
                                                                        break ;
                                                                    }
                                                                    @endphp
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--HOME DEFAULT SPORT-->
                            <div class="vk-home-section-sport">
                                <div class="tab-default tabbable-panel">
                                    <div class="tabbable-line">
                                        <div class="uni-sticker-label">
                                            <div class="label-info">
                                                <a href="{{asset('user/'. $sponewest->id)}}">
                                                    <img src="{{ asset('frontend/image/homepage1/icon/ball.png') }}" alt="" class="img-responsive img-gen">
                                                    <img src="{{ asset('frontend/image/sport-red.png') }}" alt="" class="img-responsive img-hover">
                                                    <span class="label-title">Sport</span>
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_sec-3_1">
                                                <div id="uni-home-default-fashion-newest-slide" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="vk-home-section-2-left">
                                                                    <div class="vk-sec-2-left-img">
                                                                        <a href="{{ asset('/user/view/'. $sponewest->id) }}"><img src="{{asset('uploads/' .$sponewest->image)}}" alt=""></a>
                                                                    </div>
                                                                    <div class="vk-sec-2-left-content">
                                                                        <h2><a href="{{ asset('/user/view/'. $sponewest->id) }}"> {{ substr( $sponewest->title,0,65)  }}... </a></h2>
                                                                        <div class="vk-sec-2-left-time">
                                                                            <ul>
                                                                                <li><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($news->created_at))  }}</li>
                                                                                <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="vk-sec-2-left-text">
                                                                            <p> {{ substr($sponewest->description,0,200) }}... </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                @php $i = 1; @endphp
                                                                @foreach($sports as $sport)
                                                                    <div class="vk-home-section-2-right">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="vk-sec-2-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $sport->id) }}"><img src="{{ asset('uploads/' . $sport->image) }}" alt="" class="img-responsive"></a>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-sec-2-content">
                                                                                    <div class="vk-sec-2-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $sport->id) }}"> {{ substr(  $sport->title,0,65)  }}...</a></h2>
                                                                                    </div>
                                                                                    <div class="vk-sec-2-time">
                                                                                        <span class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</span>
                                                                                    </div>
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                    if ($i== 6 || $i== 11){
                                                                        echo '</div> <div class="vk-home-section-2-right">';
                                                                        break ;
                                                                    }
                                                                    @endphp
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--HOME DEFAULT FASHION-->
                            <div class="vk-home-section-fashion">
                                <div class="tab-default tabbable-panel">
                                    <div class="tabbable-line">
                                        <div class="uni-sticker-label">
                                            <div class="label-info">
                                                <a href="{{asset('user/'. $fashnewest->id)}}">
                                                    <img src="{{asset('frontend/image/homepage1/icon/fashion.png')}}" alt="" class="img-responsive img-gen">
                                                    <img src="{{asset('frontend/image/fas-red.png')}}" alt="" class="img-responsive img-hover">
                                                    <span class="label-title">Fashion</span>
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_sec-3_1">
                                                <div id="uni-home-default-fashion-newest-slide" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="vk-home-section-2-left">
                                                                    <div class="vk-sec-2-left-img">
                                                                        <a href="{{ asset('/user/view/'. $fashnewest->id) }}"><img src="{{asset('uploads/'.$fashnewest->image)}}" alt=""></a>
                                                                    </div>
                                                                    <div class="vk-sec-2-left-content">
                                                                        <h2><a href="{{ asset('/user/view/'. $fashnewest->id) }}">{{ substr( $fashnewest->title,0,65)  }}...</a></h2>
                                                                        <div class="vk-sec-2-left-time">
                                                                            <ul>
                                                                                <li><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y, g:i a",strtotime($news->created_at))  }}</li>
                                                                                <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="vk-sec-2-left-text">
                                                                            <p> {{ substr($fashnewest->description,0,200) }}... </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                @php $i = 1; @endphp
                                                                @foreach($fashions as $fashion)
                                                                    <div class="vk-home-section-2-right">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="vk-sec-2-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $fashion->id) }}"><img src="{{asset('uploads/'.$fashion->image)}}" alt="" class="img-responsive"></a>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-sec-2-content">
                                                                                    <div class="vk-sec-2-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $fashion->id) }}"> {{ substr(  $fashion->title, 0, 65)  }}... </a></h2>
                                                                                    </div>
                                                                                    <div class="vk-sec-2-time">
                                                                                        <span class="time"><i class="fa fa-calendar" aria-hidden="true"></i> March, 20, 2017</span>
                                                                                    </div>
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                    if ($i== 6 || $i== 11){
                                                                        echo '</div> <div class="vk-home-section-2-right">';
                                                                        break ;
                                                                    }
                                                                    @endphp
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--HOME DEFAULTS MUSIC-->
                            <div class="vk-home-section-music">
                                <div class="tab-default tabbable-panel">
                                    <div class="tabbable-line">
                                        <div class="uni-sticker-label">
                                            <div class="label-info">
                                                <a href="{{asset('user/'. $musnewest->id)}}">
                                                    <img src="{{asset('frontend/image/homepage1/icon/music.png')}}" alt="" class="img-responsive img-gen">
                                                    <img src="{{asset('frontend/image/mus-red.png')}}" alt="" class="img-responsive img-hover">
                                                    <span class="label-title">Music</span>
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_sec-5_1">
                                                <div id="uni-home-default-fashion-newest-slide" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="vk-home-section-2-left">
                                                                    <div class="vk-sec-2-left-img">
                                                                        <a href="{{ asset('/user/view/'. $musnewest->id) }}"><img src="{{asset('uploads/'. $musnewest->image)}}" alt=""></a>
                                                                    </div>
                                                                    <div class="vk-sec-2-left-content">
                                                                        <h2><a href="{{ asset('/user/view/'. $musnewest->id) }}"> {{ substr(  $musnewest->title, 0, 45)  }}... </a></h2>
                                                                        <div class="vk-sec-2-left-time">
                                                                            <ul>
                                                                                <li><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y", strtotime($news->created_at))  }}</li>
                                                                                <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="vk-sec-2-left-text">
                                                                            <p> {{ substr($musnewest->description,0,200) }}... </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                @php $i = 1; @endphp
                                                                @foreach($music as $musics)
                                                                    <div class="vk-home-section-2-right">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="vk-sec-2-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $musics->id) }}"><img src="{{asset('uploads/' . $musics->image)}}" alt="" class="img-responsive"></a>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-sec-2-content">
                                                                                    <div class="vk-sec-2-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $musics->id) }}">{{ substr(  $musics->title,0,45)  }}...</a></h2>
                                                                                    </div>
                                                                                    <div class="vk-sec-2-time">
                                                                                        <span class="time"><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y",strtotime($news->created_at))  }}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                            if ($i== 6 || $i== 11){
                                                                echo '</div> <div class="vk-home-section-2-right">';
                                                                break ;
                                                            }
                                                                    @endphp
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--HOME DEFAULTS VIDEO-->
                            <div class="vk-home-section-video">
                                <div class="tab-default tabbable-panel">
                                    <div class="tabbable-line">
                                        <div class="uni-sticker-label">
                                            <div class="label-info">
                                                <a href="{{asset('user/'. $vidnewest->id)}}">
                                                    <img src="{{asset('frontend/image/homepage1/icon/video.png')}}" alt="" class="img-responsive img-gen">
                                                    <img src="{{asset('frontend/image/video-red.png')}}" alt="" class="img-responsive img-hover">
                                                    <span class="label-title">Video</span>
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_sec-6_1">
                                                <div id="uni-home-default-fashion-newest-slide" class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="vk-home-section-2-left">
                                                                    <div class="uni-play-img vk-sec-2-left-img">
                                                                        <a href="{{ asset('/user/view/'. $vidnewest->id) }}"><img src="{{asset('uploads/'. $vidnewest->image)}}" alt=""></a>
                                                                        <div class="uni-play">
                                                                            <a href="{{ asset('/user/view/'. $vidnewest->id) }}"><img src="{{asset('frontend/image/play.png')}}" alt=""></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vk-sec-2-left-content">
                                                                        <h2><a href="{{ asset('/user/view/'. $vidnewest->id) }}"> {{ substr( $vidnewest->title,0,45)  }}...</a></h2>
                                                                        <div class="vk-sec-2-left-time">
                                                                            <ul>
                                                                                <li><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y",strtotime($news->created_at))  }}</li>
                                                                                <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="vk-sec-2-left-text">
                                                                            <p> {{ substr($vidnewest->description,0,200) }}...</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                @php $i = 1; @endphp
                                                                @foreach($video as $videos)
                                                                    <div class="vk-home-section-2-right">
                                                                        <ul>
                                                                            <li>
                                                                                <div class="uni-play-img vk-sec-2-eco-img">
                                                                                    <a href="{{ asset('/user/view/'. $videos->id) }}"><img src="{{asset('uploads/'. $videos->image)}}" alt="" class="img-responsive"></a>
                                                                                    <div class="uni-play">
                                                                                        <a href="{{ asset('/user/view/'. $videos->id) }}"><img src="{{asset('frontend/image/play.png')}}" alt=""></a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="vk-sec-2-content">
                                                                                    <div class="vk-sec-2-title">
                                                                                        <h2><a href="{{ asset('/user/view/'. $videos->id) }}"> {{ substr( $videos->title,0,45)  }}...</a></h2>
                                                                                    </div>
                                                                                    <div class="vk-sec-2-time">
                                                                                        <span class="time"><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($news->created_at))  }}</span>
                                                                                    </div>
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    @php $i++;
                                                                    if ($i== 6 || $i== 11){
                                                                        echo '</div> <div class="vk-home-section-2-right">';
                                                                        break ;
                                                                    }
                                                                    @endphp
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <aside class="col-md-4">
                        <aside class="widget-area">

                            <aside class="widget">
                                <div class="widget-content">
                                    <div class="vk-home-default-right-ad">
                                        <a href="#">
                                            <img src="{{asset('frontend/image/ad-sidebar.jpg')}}" alt="ad-sidebar" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </aside>

                            <aside class="widget">
                                <h3 class="widget-title">Editor Picks</h3>
                                <div class="widget-content">
                                    <div class="vk-home-default-right-ep">
                                        <div id="vk-owl-ep-slider" class="owl-carousel owl-theme">
                                            <div class="item">
                                                @php $i = 1; @endphp
                                                @if(count($pics)>0)
                                                    @foreach($pics as $news)
                                                        <ul>
                                                            <li>
                                                                <div class="vk-item-ep">
                                                                    <div class="vk-item-ep-img">
                                                                        <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('uploads/' . $news->image)}}" alt="img"></a>
                                                                    </div>
                                                                    <div class="vk-item-ep-text">
                                                                        <h2><a href="{{ asset('/user/view/'. $news->id) }}">{{ substr( $news->title,0,45)  }}...</a></h2>
                                                                        <div class="vk-item-ep-time">
                                                                            <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i>  {{ date("F j, Y, g:i a",strtotime($news->created_at))  }}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </li>
                                                        </ul>
                                                        @php $i++;
                                                                  if($i == 6 || $i == 11) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    {{ 'News not Found' }}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>

                            <aside class="widget">
                                <div class="widget-content">
                                    <div class="vk-home-default-right-facebook">
                                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&amp;tabs=timeline&amp;width=340&amp;height=500&amp;small_header=false&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId" width="340" height="500" style="border:none;overflow:hidden"></iframe>
                                    </div>
                                </div>
                            </aside>

                            <aside class="widget">
                                <h3 class="widget-title">Feature Videos</h3>
                                <div class="widget-content">
                                    <div class="vk-home-default-right-fea">
                                        <div id="vk-owl-fea-slider" class="owl-carousel owl-theme">
                                            <div class="item">
                                                @php $i = 1; @endphp
                                                @if(count($video)>0)
                                                    @foreach($video as $news)
                                                        <ul>
                                                            <li>
                                                                <div class="vk-item-fea">
                                                                    <div class="uni-play-img vk-item-fea-img">
                                                                        <!--                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/v-Dur3uXXCQ" frameborder="0" allowfullscreen></iframe>-->
                                                                        <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('uploads/' . $news->image)}}" alt="img-2"></a>
                                                                        <div class="uni-play">
                                                                            <a href="{{ asset('/user/view/'. $news->id) }}"><img src="{{asset('frontend/image/play.png')}}" alt="" class="img-responsive"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vk-item-fea-text">
                                                                        <h2><a href="{{ asset('/user/view/'. $news->id) }}"> {{ substr( $news->title,0,75)  }}...</a></h2>
                                                                        <div class="vk-item-fea-time">
                                                                            <ul>
                                                                                <li><i class="fa fa-calendar" aria-hidden="true"></i> {{ date("F j, Y",strtotime($news->created_at))  }}</li>
                                                                                <li><i class="fa fa-eye-slash" aria-hidden="true"></i> 2134 views</li>
                                                                                <li><i class="fa fa-comments-o" aria-hidden="true"></i> 30 Comments</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </li>
                                                        </ul>
                                                        @php $i++;
                                                                  if($i == 4 || $i == 7) {
                                                                    echo '</div> <div class="item">';
                                                                  }
                                                        @endphp
                                                    @endforeach
                                                @else
                                                    {{ 'News not Found' }}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>

                            <aside class="widget">
                                <h3 class="widget-title">Stay Connected</h3>
                                <div class="widget-content">
                                    <div class="vk-home-default-right-stay">
                                        <div class="vk-right-stay-body">
                                            <a class="btn btn-block btn-social btn-facebook">
                                                <span class="icon"><i class="fa fa-facebook"></i></span>
                                                <span class="info"> 2134 Like</span>
                                                <span class="text">Like</span>
                                            </a>
                                            <a class="btn btn-block btn-social btn-twitter">
                                                <span class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                <span class="info"> 13634 Follows</span>
                                                <span class="text">Follows</span>
                                            </a>
                                            <a class="btn btn-block btn-social btn-youtube">
                                                <span class="icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
                                                <span class="info">10634 Subscribers</span>
                                                <span class="text">Subscribe</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </aside>

                            <aside class="widget">
                                <div class="widget-content">
                                    <div class="vk-home-default-right-ad">
                                        <a href="#">
                                            <img src="{{asset('frontend/image/ad-sidebar.jpg')}}" alt="ad-sidebar" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </aside>

                            <aside class="widget">
                                <h3 class="widget-title">Tags Clound</h3>
                                <div class="widget-content">
                                    <div class="vk-home-default-right-tags">
                                        @php $i = 1; @endphp
                                        @foreach($data as $tag)
                                            <ul>
                                                <li><a href="#">{{$tag->tagName }}</a></li>

                                            </ul>
                                            @php $i++; @endphp
                                        @endforeach
                                    </div>
                                </div>
                            </aside>

                        </aside>
                    </aside>
                </div>
            </div>
        </div>
    </div>
@endsection()
