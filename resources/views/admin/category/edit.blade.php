
@extends('admin.layouts.master')

@section('content')

    <div class="right_col" role="main" style="min-height: 3771px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Form Elements</h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Form Design</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br>
                            {!! Form::open([ 'url'=>'/admin/category/index', 'method' => 'patch', 'enctype'=> 'multipart/from-data', 'id' => 'demo-form2', 'class' => 'form-horizontal form-label-left']) !!}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Sub-Category Name :</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{--{{ dd($categories) }}--}}
                                        @foreach($categories as $sub_categories)

                                                <input  type="text" id="first-name" name="sub_category" value="{{ $sub_categories->sub_category}}" required="required" class="form-control col-md-7 col-xs-12">

                                        @endforeach

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-primary" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection