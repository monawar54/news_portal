@extends('admin.layouts.master')

@section('content')
    <div class="right_col" role="main" style="min-height: 3771px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Form Elements</h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Category Content </h2>
            <div class="clearfix"></div>
            </div>
                <div class="x_content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th> Category Name</th>
                                {{--<th> Sub-Category Name</th>--}}
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                        @php $i = 1; @endphp
                        @foreach($data as $category)
                            <tr>
                                <td>{{$i++}}</td>
                               <td>{{$category->catName}}</td>
                                {{--@foreach($subcat as $sub_categories)
                                    <td>{{$sub_categories->sub_category}}</td>
                                @endforeach--}}
                                <td>
                                    <a class="btn btn-danger" href="{{ url('/admin/category/'. $category->id) }}"> Delete </a>
                                    <a class="btn btn-primary" href="{{ url('/admin/category/edit/'. $category->id) }}"> Edit </a>
                               </td>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection