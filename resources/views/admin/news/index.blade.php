@extends('admin.layouts.master')

@section('content')
    <div class="right_col" role="main" style="min-height: 3771px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Form Elements</h3>

                    @if(session()->has('message'))
                        {{ session('message') }}
                    @endif

                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>News Content table </h2>
            <div class="clearfix"></div>
            </div>
                <div class="x_content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th> News Title </th>
                                <th>Category</th>
                                <th>Descirption</th>
                                <th>Images</th>
                            </tr>
                        </thead>

                        <tbody>
                        @php $i = 1; @endphp
                       @foreach($data as $news)
                           <tr>
                              <td>{{$i++}}</td>
                                <td>{{ substr($news->title, 0,30) }}</td>
                                <td>{{ $news->category }}</td>
                                <td>{{ substr($news->description, 0,40) }}</td>
                                <td>
                                    <img width="100" src="{{asset('uploads/'.$news->image)}}" alt="{{ $news->title}}">
                                </td>
                                <td >
                                    <div class="btn-group  btn-group-sm" style="color: white">
                                        <button class="btn btn-success" type="button"> <a href="{{ url('/admin/news/show/'. $news->id) }}" style="color: white"><i class="fa fa-eye"></i></a></button>
                                        <button class="btn btn-primary" type="button"><a href="{{ url('/admin/news/edit/'. $news->id) }}" style="color: white"><i class="fa fa-edit"></i></a></button>
                                        <button class="btn btn-danger" type="button"><a href="{{ url('/admin/news/'. $news->id) }}" style="color: white"><i class="fa fa-trash"></i></a></button>

                                       {{-- <form action="{{ url('/admin/news/'. $news->id) }}" method="delete" class="btn btn-danger" ><i class="fa fa-trash"></i>
                                            <button  type="button" value="delete"></button>
                                        </form>--}}


                                        {{--{!! Form::open(['url' => '/admin/news/'. $news->id, 'method' => 'delete']) !!}
                                        {!! <a href="{{ route('/admin/news/'. $news->id)}}"><i class="fa fa-trash">
                                        </a> !!}

                                        {{ Form::button(['class'=>'btn btn-danger', 'type' => 'button','value' => 'delete'])
                                         }}

                                        {!! Form::close() !!}--}}


                                    </div>
                                </td>
                           </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection