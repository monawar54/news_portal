<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;


class SliderController extends Controller
{
    //
    public function create()
    {
        $data = DB::table('category')->get();
        return view('admin.slider.create', compact('data'));
    }
}

