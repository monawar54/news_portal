<?php

namespace App\Http\Controllers;

use App\Roll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    //
    public function create()
    {
        //$data = DB::table('user_roll')->get();
        return view('admin.user.create', compact('data'));
    }

    public function store(Request $request)
    {


        DB::table('user_roll')->insert(
            [
                'name' => $request->name,
                'mail' => $request->mail,
                'password' => $request->password,
                'type' => $request->type
            ]
        );

        return redirect()->back();
    }
}
