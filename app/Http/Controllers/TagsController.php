<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagsController extends Controller
{
    public function index()
    {
        $data = DB::table('tags')->get();
        return view ('admin.tags.create', compact('data'));

    }
    public function store(Request $request)
    {
        DB::table('tags')->insert(
            ['tagName' => $request->tagName]
        );

        return redirect()->back();
    }
}
