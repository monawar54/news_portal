<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\News;

class NewsController extends Controller
{
    public function create()
    {
        $data = DB::table('category')->get();
        return view('admin.news.create', compact('data'));
    }

    public function show($id)
    {
        $data = DB::table('news')
            ->select('news.*',DB::raw("group_concat(category.catName) as category")) // (news) and (category) table er data grouply concated
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')     //
            ->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->groupby('news.id')
            ->where('news.id', $id) // show for catch the url->id and newsTable->id
            ->first(); // show for only one data
        return view('admin.news.show', compact('data'));
    }

    public function index()
    {
        $data = DB::table('news')
            ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->groupby('news.id')
            ->get(); // show for all data

        return view('admin.news.index', compact('data'));
    } // OrderedBy category wise news uploaded

    public function store(Request $request)
    {
        if($request->hasFile('image')){
            $file       = $request->file('image'); // catching inserted file (image)
            $extension  = $file->getClientOriginalExtension(); // Catching image extension
            $filename= substr(md5(time()),0,10).".".$extension;
            $file->move(public_path('\uploads'), $filename); //For move upload file
        } //image uploading for news

        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'image' => $filename,
            'tags' => $request->tagName
        ];
        if(News::create($data))
        {
           $news = DB::table('news')->orderBy('created_at', 'desc')->first();
           foreach ($request->category_id as $category_id)
           {
               DB::table('category_news')->insert([
                   'category_id' =>  $category_id,
                   'news_id' =>  $news->id
               ]);
           } //mapping table content

           session()->flash('message', 'Data Inserted Successfully'); // message showing
            return redirect('/admin/news/create');

        }
    }
    public function edit($id)
    {
        $data = DB::table('news')
            ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->groupby('news.id')
            ->where('news.id', $id)
            ->first();

        $categories = DB::table('category')->get();
        return view('admin.news.edit', compact('data','categories'));
    }
    public function update(Request $request)

    {
        if($request->hasFile('image')){
            $file       = $request->file('image'); // catching inserted file (image)
            $extension  = $file->getClientOriginalExtension(); // Catching image extension
            $filename = substr(md5(time()),0,10).".".$extension;
            $file->move(public_path('\uploads'), $filename); //For move upload file
            $request->image = $filename;

        } // update image upload for news

        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'image' => $request->image->fit(200,150)->save(),
            'tags' => $request->tags
        ];

        DB::table('news')->where('id', $request->id)->update($data);

        session()->flash('message', 'News Updated Successfully'); // for messaging
        return redirect('/admin/news/index');

    }
    public function destroy($id)
    {
        DB::table('category_news')->where('news_id', $id)->delete();
        DB::table('news')->where('id', $id)->delete();
        return redirect('/admin/news/index');
    }
}
