<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Search;
use App\News;


class FrontPageController extends Controller
{
    public function index()
    {
        /*$all = DB::table('news')
            ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->groupby('news.id')
            ->orderBy('id','desc')
            ->get(); // OrderedBy category wise news uploaded*/

        $category = DB::table('category')
            ->select('category.*',DB::raw("group_concat(sub_categories.sub_category) as sub_category"))
            ->leftJoin('sub_categories', 'category.id', '=', 'sub_categories.category_id')
            ->groupby('category.id')
            ->get();  // OrderedBy Sub-category wise news uploaded
        $latest = DB::table('news')
            // ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '93')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        $economy = DB::table('news')
            // ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '86')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded
        $econewest = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '86')
            ->orderBy('news_id','desc')
            ->first();

        $sports = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '87')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        $sponewest = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '87')
            ->orderBy('news_id','desc')
            ->first();

        $fashions = DB::table('news')
            // ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '88')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        $fashnewest = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '88')
            ->orderBy('news_id','desc')
            ->first();

        $music = DB::table('news')
            // ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '89')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded
        $musnewest = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '89')
            ->orderBy('news_id','desc')
            ->first();

        $video = DB::table('news')
            // ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '90')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        $vidnewest = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '90')
            ->orderBy('news_id','desc')
            ->first();

        $pics = DB::table('news')
            // ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '92')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        $data = DB::table('tags')->get();

        return view('index', compact('latest','data', 'category','pics',
                'economy','econewest',
                'sports','sponewest',
                'fashions','fashnewest',
                'music','musnewest',
                'video','vidnewest','all')
        ); //comes data with array
    }
    public function category_news($id)
    {
        $category_news = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')     //
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', $id) // show for catch the url->id and newsTable->id
            ->orderBy('news_id','desc')
            ->get(); // show for only one data
        $pics = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '92')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded
        $video = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '90')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        $category = DB::table('category')
            ->select('category.*',DB::raw("group_concat(sub_categories.sub_category) as sub_category"))
            ->leftJoin('sub_categories', 'category.id', '=', 'sub_categories.category_id')
            ->groupby('category.id')
            ->get();  // OrderedBy Sub-category wise news uploaded
        $latest = DB::table('news')
            // ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '93')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        return view('user.economy', compact('category_news','economy','pics','video','category','latest'));
    }

    public function show($id)
    {
        $data = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')     //
            ->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            //->leftJoin('sub_categories', 'category.id', '=', 'sub_categories.category_id')
            ->where('category_news.id', $id) // show for catch the url->id and newsTable->id
            //->orwhere('news.id', $id)
            ->first(); // show for only one data
//dd($data);
        $category = DB::table('category')
            ->select('category.*',DB::raw("group_concat(sub_categories.sub_category) as sub_category"))
            ->leftJoin('sub_categories', 'category.id', '=', 'sub_categories.category_id')
            ->groupby('category.id')
            ->get();

        $pics = DB::table('news')
            // ->select('news.*',DB::raw("group_concat(category.catName) as category"))
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '92')
            ->orderBy('news_id','desc')
            ->get();
        $video = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '90')
            ->orderBy('news_id','desc')
            ->get();
        $economy = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '86')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded
        $econewest = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            ->where('category_news.category_id', '=', '86')
            ->orderBy('news_id','desc')
            ->first();

        $latest = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '93')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        return view('user.view', compact('data', 'category','latest','pics','video','economy','econewest'));
    }

    public function search(Request $request)
    {
        //dd($request);

        $result = Search::search(
            "news" ,
            ['title' , 'description', 'image'] ,
            "$request",
            ['id', 'title', 'description','image'],
            ['id'  , 'asc'] ,
            true ,
            30
        );

        //dd($result);




       /* $search=$request->title;
        $result= DB::table('news')->where('title', 'like', "$search");*/


       /* $q = Input::get('search');
            $search = News::where('title', 'LIKE', '%' . $q . '%')
                ->orWhere('description', 'LIKE', '%' . $q . '%');

            foreach($search as $result){
                dd($result->title);
            }
            if (count($search) > 0){
                return view('index')->withDetails($search)
                ->withQuery($q);
            }*/

        $category = DB::table('category')
            ->select('category.*',DB::raw("group_concat(sub_categories.sub_category) as sub_category"))
            ->leftJoin('sub_categories', 'category.id', '=', 'sub_categories.category_id')
            ->groupby('category.id')
            ->get();

        $latest = DB::table('news')
            ->leftJoin('category_news', 'news.id', '=', 'category_news.news_id')
            //->leftJoin('category', 'category_news.category_id', '=', 'category.id')
            ->where('category_news.category_id', '=', '93')
            ->orderBy('news_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        return view('user.search', compact('category','latest','result'));
    }


}
