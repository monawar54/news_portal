<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UserLogin;
class UserLoginController extends Controller
{
    public function login(){
        return view('user.login');
    }

    /*public function register(){
        return view('user.register');
    }*/

    public function create()
    {
        DB::table('userslogin')->get();
        return view('user.register');
    }


    public function store(Request $request)
    {

        DB::table('userslogin')->insert(
            [
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'remember_token' => $request->remember_token
            ]
        );

        return redirect()->back();
    }
}
