<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$data = Category::get();
        return view('admin.category.index', compact('data'));*/
         $data =   DB::table('category')->get();
         return view('admin.category.index' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = DB::table('category')->get();
        return view('admin.category.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // For Main cetygory insert
        if(isset($request->catName)){
            DB::table('category')->insert(
                ['catName' =>  $request->catName]
            );
        }
        // sub category insert
        if(isset($request->sub_category)){
           DB::table('sub_categories')->insert(
                ['sub_category' =>  $request->sub_category,
                    'category_id' =>  $request->input('category_id')]
            );
        }

        session()->flash('message', 'Data Inserted Successfully'); // message showing
        return redirect('/admin/category/create');

    }

    /*
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
        $categories = DB::table('category')
            ->select('category.*', DB::raw("group_concat(sub_categories.sub_category) as sub_category"))
            ->leftJoin('sub_categories', 'category.id', '=', 'sub_categories.category_id')
            ->groupby('category.id')
            ->where('category.id', $id)
            ->get();


        return view('admin.category.edit', compact('categories'));
    }
    /*
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(isset($request->catName)){
            DB::table('category')->insert(
                ['catName' =>  $request->catName]
            );
        }
        if(!empty($request->sub_category)){
            DB::table('sub_categories')->insert(
                ['sub_category' =>  $request->sub_category,
                    'category_id' =>  $request->category_id]
            );
        }

        DB::table('sub_categories')->where('id',$request->id)->update();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('category_news')->where('category_id', $id)->delete();
        DB::table('sub_categories')->where('category_id', $id)->delete(); //
        DB::table('category')->where('id', $id)->delete();
        return redirect('/admin/category/index');
    }
}
