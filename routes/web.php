<?php

// User module
Route::get('/', 'FrontPageController@index');
Route::post('/search', 'FrontPageController@search');
Route::get('/user/view/{id}', 'FrontPageController@show');
Route::get('/user/{id}', 'FrontPageController@category_news');


//User login module
Route::get('user/login', 'UserLoginController@login');
Route::get('user/register', 'UserLoginController@create');
//Route::get('user/register', 'UserLoginController@store');

// Admin login module
Route::get('/admin/user/create', 'UserController@create');
Route::post('/admin/user/store', 'UserController@store');

// Admin authentication of admin
Auth::routes();
Route::get('/admin', 'HomeController@index');

/* Admin dashboard */
Route::get('/admin', function () {
    return view('admin.index');
});

/* Admin Category Module */
Route::get('/admin/category/create', 'CategoryController@create');
Route::post('/admin/category/create', 'CategoryController@store');
Route::get('/admin/category/index', 'CategoryController@index');
Route::get('/admin/category/{id}', 'CategoryController@index');
Route::get('/admin/category/edit', 'CategoryController@edit');
Route::get('/admin/category/edit/{id}', 'CategoryController@edit');
Route::patch('/admin/category/index', 'CategoryController@update');
Route::get('/admin/category/{id}', 'CategoryController@destroy');

/* Admin Tags Module */
Route::get('/admin/tags/create', 'TagsController@index');
Route::post('/admin/tags/store', 'TagsController@store');

/* Admin News Module */
Route::get('/admin/news/create', 'NewsController@create');
Route::post('/admin/news/store', 'NewsController@store');
Route::get('/admin/news/index', 'NewsController@index');
Route::get('/admin/news/show', 'NewsController@show');
Route::get('/admin/news/show/{id}', 'NewsController@show');
Route::get('/admin/news/edit', 'NewsController@edit');
Route::get('/admin/news/edit/{id}', 'NewsController@edit');
Route::patch('/admin/news', 'NewsController@update');
Route::get('/admin/news/{id}', 'NewsController@destroy');

// Admin slider
Route::get('/admin/slider/create', 'SliderController@create');




